'use strict'

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-cssmin');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var del = require('del');
var runSequence = require('run-sequence');
var replace = require('gulp-replace');
var nunjucksRender = require('gulp-nunjucks-render');

gulp.paths = {
    dist: 'dist',
    vendors: 'dist/vendor',
    src: 'src'
};

var paths = gulp.paths;
/*
var vendorsJS = [
  'node_modules/bootstrap/dist/js/bootstrap.min.js',
  'node_modules/bootstrap-daterangepicker/daterangepicker.js',
  'node_modules/chart.js/dist/Chart.min.js',
  'node_modules/datatables.net/js/jquery.dataTables.js',
  'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
  'node_modules/fullcalendar/dist/fullcalendar.min.js',
  'node_modules/fullcalendar/dist/gcal.min.js',
  'node_modules/gaugeJS/dist/gauge.min.js',
  'node_modules/ion-rangeslider/js/ion.rangeSlider.min.js',
  'node_modules/jquery/dist/jquery.min.js',
  'node_modules/jquery/dist/jquery.min.map',
  'node_modules/jquery-ui-dist/jquery-ui.min.js',
  'node_modules/jquery-validation/dist/jquery.validate.min.js',
  'node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
  'node_modules/ladda/dist/ladda.min.js',
  'node_modules/ladda/dist/spin.min.js',
  'node_modules/moment/min/moment.min.js',
  'node_modules/quill/dist/quill.min.js',
  'node_modules/quill/dist/quill.min.js.map',
  'node_modules/pace-progress/pace.min.js',
  'node_modules/popper.js/dist/umd/popper.min.js',
  'node_modules/popper.js/dist/umd/popper.min.js.map',
  'node_modules/select2/dist/js/select2.min.js',
  'node_modules/toastr/toastr.js'
]
*/

gulp.task('minify:JS', function() {
  return gulp.src([paths.src + '/js/*.js'])
  .pipe(uglify())
  .pipe(gulp.dest(paths.dist+'/js/'));
});

gulp.task('clean:dist', function () {
    return del(paths.dist);
});

gulp.task('copy:img', function() {
   return gulp.src(paths.src+'/img/**/*')
   .pipe(gulp.dest(paths.dist+'/img'));
});

gulp.task('copy:vendor', function() {
   return gulp.src(paths.src+'/vendor/**/*')
   .pipe(gulp.dest(paths.dist+'/vendor/'));
});

gulp.task('copy:device-mockups', function() {
   return gulp.src(paths.src+'/device-mockups/**/*')
   .pipe(gulp.dest(paths.dist+'/device-mockups/'));
});

gulp.task('nunjucks:dist', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src(paths.src+'/pages/*.html')
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: [paths.src+'/templates']
    }))
  // output files in app folder
  .pipe(gulp.dest('dist/'));
});

gulp.task('minify:sass', function () {
  return gulp.src(paths.src+'/scss/style.scss')
  .pipe(sass())
  .pipe(cssmin())
  .pipe(gulp.dest(paths.dist+'/css'));
});

gulp.task('build:dist', function(callback) {
    runSequence('clean:dist', 'minify:sass', 'copy:img', 'minify:JS', 'copy:device-mockups', 'copy:vendor', 'nunjucks:dist', callback);
});
