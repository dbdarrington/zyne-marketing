# README #

### What is this repository for? ###

* The front end website and marketing page

### How do I get set up? ###

#### To Test: ####
Go to the root directory
Run "npm install"
Run "gulp"
Go to localhost:3000
Code gets built in the /src folder and served from there

#### To Deploy: ####
Run "gulp build:dist"
Everything gets built into the /dist folder