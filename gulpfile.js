'use strict'

var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin')
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var nunjucksRender = require('gulp-nunjucks-render');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');

require('require-dir')('./gulp-tasks');

gulp.paths = {
  dist: 'dist',
  src: 'src',
  vendor: 'src/vendor'
};

var paths = gulp.paths;

// Static Server + watching scss/html files
gulp.task('serve', ['nunjucks','sass'], function() {

  browserSync.init({
    server: "src/"
  });

  gulp.watch(['src/pages/**/*.*','src/templates/**/*.*'],['nunjucks']);
  gulp.watch('src/scss/**/*.scss', ['sass']);
  gulp.watch('src/**/*.html').on('change', browserSync.reload);
  gulp.watch('src/js/**/*.js').on('change', browserSync.reload);
  gulp.watch('src/css/*.css').on('change', browserSync.reload);

});

gulp.task('sass', function () {
  return gulp.src(paths.src+'/scss/style.scss')
  .pipe(plumber({errorHandler: onError}))
  .pipe(sass())
  .pipe(gulp.dest(paths.src+'/css'))
  .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);

gulp.task('nunjucks', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src(paths.src+'/pages/*.html')
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: [paths.src+'/templates']
    }))
  // output files in app folder
  .pipe(gulp.dest('src/'))
  .pipe(browserSync.stream());
});

var onError = function (err) {
  gutil.beep();
  console.log(err);
};
